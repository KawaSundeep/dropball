Repo name: DropBall
Content: Android Code
Type: Game
email: sundeep@kawanan.com

It's a repository used to push my code to bitbucket.
the code is all about a simple android game which is my first android application. 

Developed to learn developing games on android.
To add more for this, reading data from android mobile sensors which is used to give motion to the game.

The game is all about dropping a ball by bending mobile left/right from a slot moving upwards.
as the user plays, speed increases as time and score to make the game difficult and to move your hands fast.

This is my first android application developed with the help of my Bosses(no friends...)