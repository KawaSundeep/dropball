package com.game.ball;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class OptionsActivity extends Activity implements OnClickListener {
	ImageView soundImg, startImg;
	TextView startText, highScoreText;
	Context context;
	SharedPreferences pref;
	boolean volume;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		context = getApplicationContext();
		setContentView(R.layout.optionsview);
		pref = getSharedPreferences(Constants.PREF_TITLE, 0);
		startImg = (ImageView) findViewById(R.id.startImage);
		startText = (TextView) findViewById(R.id.startText);
		highScoreText = (TextView) findViewById(R.id.highScore);
		highScoreText.setText("Challenging score: "
				+ pref.getInt(Constants.PREF_KEY_TOP_SCORER, 0));
		soundImg = (ImageView) findViewById(R.id.soundImg);
		// checking volume status
		volume = pref.getBoolean(Constants.PREF_KEY_VOLUME, true);
		if (!volume) {
			soundImg.setImageResource(R.drawable.nosound);
		}
		startImg.setOnClickListener(this);
		startText.setOnClickListener(this);
		soundImg.setOnClickListener(this);
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.soundImg:
			if (volume) {
				soundImg.setImageResource(R.drawable.nosound);
				volume = false;
				pref.edit().putBoolean(Constants.PREF_KEY_VOLUME, false)
						.commit();
			} else {
				soundImg.setImageResource(R.drawable.soundon);
				volume = true;
				pref.edit().putBoolean(Constants.PREF_KEY_VOLUME, true)
						.commit();
			}
			break;
		default:
			startActivity(new Intent(context, DropMeActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {

		super.onResume();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}
}
