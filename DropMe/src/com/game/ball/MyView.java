package com.game.ball;

import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.view.View;

public class MyView extends View {
	public boolean isFirst = true;
	public boolean isDraw = false;
	public boolean isGameOver = false;
	private int lineBreakAt, lineHeight, viewHeight, viewWidth;
	private int imageX, imageY, imageWidth, imageHeight;
	private int lineSpeed;
	private int score;
	private int scoreDiff;
	private Random random;
	private Paint paint;
	private Bitmap image;
	private MediaPlayer player;
	private SharedPreferences pref;

	public MyView(Context context) {
		super(context);
		pref = context.getSharedPreferences(Constants.PREF_TITLE, 0);
		if (pref.getBoolean(Constants.PREF_KEY_VOLUME, true)) {
			player = MediaPlayer.create(context, R.raw.achu);
		}
		paint = new Paint();
		paint.setColor(Color.rgb(0, 153, 255));
		random = new Random();
		image = Bitmap.createBitmap(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.ball));
		imageWidth = image.getWidth();
		imageHeight = image.getHeight();
		setKeepScreenOn(true);
		setBackgroundColor(Color.BLACK);

	}

	@Override
	public void onDraw(Canvas canvas) {
		if (isFirst) {
			viewHeight = getHeight();
			viewWidth = getWidth();
			startNewGame();
			isFirst = false;
		}
		canvas.drawText("Score: " + score, 0, 10, paint);
		canvas.drawLine(0, lineHeight, lineBreakAt, lineHeight, paint);
		canvas.drawLine(lineBreakAt + imageWidth + 20, lineHeight, viewWidth,
				lineHeight, paint);
		canvas.drawBitmap(image, imageX, imageY, paint);
		score++;
		if ((score - scoreDiff) > 400 && lineSpeed < 5) {
			lineSpeed++;
			scoreDiff = score;
		}
		isDraw = true;
	}

	public void update() {
		isDraw = false;
		if (lineHeight >= 0) {
			lineHeight -= lineSpeed;
		} else {
			lineHeight = viewHeight;
			lineBreakAt = random.nextInt(viewWidth - imageWidth - 20);
		}
		// ball x, y calculation
		if (DropMeActivity.isLeft) {
			if (imageX > 3) {
				imageX -= 3;
			}
		} else {
			if ((imageX + imageWidth) < viewWidth - 3) {
				imageX += 3;
			}
		}
		int whereIsBall = (lineHeight - (imageY + imageHeight));
		if (whereIsBall > 5) {
			// above the line
			imageY += 5;
		} else if (whereIsBall < 5 && whereIsBall >= -5) {
			// on the line;
			if ((imageX < lineBreakAt)
					|| ((imageX + imageHeight) > (lineBreakAt + imageWidth + 20))) {
				imageY = lineHeight - imageHeight;
			}
			if (imageY <= 0) {
				if (player != null) {
					player.start();
				}
				pref.edit().putInt(Constants.PREF_KEY_SCORE, score).commit();
				isGameOver = true;
			}
		} else {
			// below the line
			if ((imageY + imageHeight) < (viewHeight - 5))
				imageY += 5;
		}
	}

	public void startNewGame() {
		isFirst = true;
		isDraw = false;
		isGameOver = false;
		lineHeight = viewHeight;
		lineBreakAt = viewWidth / 2;
		imageX = viewWidth / 2;
		imageY = 0;
		lineSpeed = 1;
		score = 0;
		scoreDiff = 0;
	}
}
