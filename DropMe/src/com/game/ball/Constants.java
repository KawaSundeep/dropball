package com.game.ball;

public class Constants {

	static final String APP_NAME = "Drop fast";

	static final String TAG_OPTIONS_ACTIVITY = "OptionsActivity";
	static final String TAG_DROP_ME_ACTIVITY = "DropMeActivity";
	static final String TAG_MY_VIEW = "MyView";

	static final String PREF_TITLE = "DropMePref";
	static final String PREF_KEY_DOWNLOADED_ON = "DownloadedOn";
	static final String PREF_KEY_OPENED_AT = "OpenedAt";
	static final String PREF_KEY_CLOSED_AT = "ClosedAt";
	static final String PREF_KEY_GAME_FLOW = "GameFlow";
	static final String PREF_KEY_VOLUME = "Volume";
	static final String PREF_KEY_SCORE = "Score";
	static final String PREF_KEY_TOP_SCORER = "TopScore";

}
