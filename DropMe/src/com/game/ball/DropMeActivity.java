package com.game.ball;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class DropMeActivity extends Activity {

	static Timer myTimer;
	static float lastPosition = 0;
	static boolean isLeft = false;
	private SensorManager sensorManager;
	private Sensor sensor;
	private MyView view;

	private Context context = this;
	private MediaPlayer player;
	private SharedPreferences pref;
	private SensorEventListener listener = new SensorEventListener() {

		@Override
		public void onSensorChanged(SensorEvent event) {
			float x = event.values[0];
			if (x > 0) {
				isLeft = true;
			} else if (x < 0) {
				isLeft = false;
			}
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
	};

	public Handler updateHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (view.isFirst) {
				view.invalidate();
			} else {
				view.invalidate();
				view.update();
			}
			super.handleMessage(msg);
		}
	};

	private TimerTask myTask = new TimerTask() {
		@Override
		public void run() {
			if (view.isDraw && !view.isGameOver) {
				updateHandler.sendEmptyMessage(0);
			} else if (view.isGameOver) {
				this.cancel();
				sensorManager.unregisterListener(listener);
				if (player != null) {
					player.stop();
				}
				this.cancel();
				startActivity(new Intent(context, GameStatusActivity.class));
			}
		}
	};

	/** Called when the activity is first created. */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		pref = getSharedPreferences(Constants.PREF_TITLE, 0);
		if (pref.getBoolean(Constants.PREF_KEY_VOLUME, true)) {
			player = MediaPlayer.create(context, R.raw.background);
			player.setLooping(true);
			player.start();
		}
		view = new MyView(context);
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		sensor = sensorManager
				.getDefaultSensor(SensorManager.SENSOR_ORIENTATION);

		setContentView(view);
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		sensorManager.registerListener(listener, sensor,
				SensorManager.SENSOR_DELAY_NORMAL);
		myTimer = new Timer();
		myTimer.schedule(myTask, 0, 20);
		Log.i("Activity", "onResume");
		super.onResume();
	}

	@Override
	protected void onStop() {
		myTimer.cancel();
		sensorManager.unregisterListener(listener);
		if (player != null) {
			player.stop();
		}
		super.onStop();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

	}
}
