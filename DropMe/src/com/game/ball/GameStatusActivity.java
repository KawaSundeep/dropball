package com.game.ball;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class GameStatusActivity extends Activity implements OnClickListener {
	private SharedPreferences pref;
	private TextView gameStatus, gameScore, back, chFriend;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.gamestatus);
		gameStatus = (TextView) findViewById(R.id.gameStatusText);
		gameScore = (TextView) findViewById(R.id.gameScore);
		chFriend = (TextView) findViewById(R.id.challengeFriend);
		back = (TextView) findViewById(R.id.backToMenu);
		back.setOnClickListener(this);
		chFriend.setOnClickListener(this);
		pref = getSharedPreferences(Constants.PREF_TITLE, 0);
		int topScore = pref.getInt(Constants.PREF_KEY_TOP_SCORER, 0);
		int score = pref.getInt(Constants.PREF_KEY_SCORE, 0);

		if (score > topScore) {
			pref.edit().putInt(Constants.PREF_KEY_TOP_SCORER, score).commit();
			gameStatus.setText("Wooo! you move fast...");
		} else {
			gameStatus.setText("Oooo! let's drop again...");
		}
		gameScore.setText("score: " + score);
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(getApplicationContext(),
				OptionsActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		super.onBackPressed();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.backToMenu:
			finish();
			break;
		case R.id.challengeFriend:
			// TODO:(sundeep) open email intent.
			Intent emailIntent = new Intent(Intent.ACTION_SEND);
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "DropBall Invitation");
			emailIntent.setType("text/plain");
			startActivity(Intent.createChooser(emailIntent, "Invite mail"));
			break;
		}
	}

}
